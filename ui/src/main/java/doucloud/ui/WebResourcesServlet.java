package doucloud.ui;

import org.apache.commons.io.IOUtils;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
@Singleton
public class WebResourcesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String resourcePath = req.getPathInfo();
        URL resourceUrl = getResourceUrl(resourcePath);
        if (resourceUrl == null) {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            resp.setStatus(HttpServletResponse.SC_OK);
            String mimeType = getServletContext().getMimeType(resourcePath);
            resp.setContentType(mimeType);

            InputStream resourceStream = null;
            OutputStream outputStream = null;

            try {
                outputStream = resp.getOutputStream();
                resourceStream = resourceUrl.openStream();

                IOUtils.copy(resourceStream, outputStream);
            } finally {
                IOUtils.closeQuietly(resourceStream);
                IOUtils.closeQuietly(outputStream);
            }
        }
    }

    private URL getResourceUrl(String resourcePath) {
        if (resourcePath.contains("../")) {     //prevent from clever users
            return null;
        } else {
            return getClass().getResource("/doucloud/ui/assets" + resourcePath);
        }
    }
}
