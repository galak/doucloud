package doucloud.ui;

import doucloud.ui.util.VelocityBasedServlet;
import duocloud.core.MemberInfo;
import duocloud.core.MembershipService;
import org.apache.velocity.VelocityContext;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * @author Anatoliy Sokolenko
 */
@Singleton
public class MembershipServlet extends VelocityBasedServlet {
    @Inject
    private MembershipService membershipService;

    public MembershipServlet() {
        super("membership.vm");
    }

    @Override
    protected void prepareContext(HttpServletRequest req, HttpServletResponse resp, VelocityContext ctx) throws IOException {
        Set<MemberInfo> members = membershipService.getMembers();

        ctx.put("members", members);
    }
}
