package doucloud.ui.navigation;

import org.apache.commons.lang.StringUtils;

import javax.inject.Singleton;
import java.util.AbstractList;
import java.util.List;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
@Singleton
public class Navigator {
    private final List<NavigationItem> navigationItems;

    public Navigator(List<NavigationItem> navigationItems) {
        this.navigationItems = navigationItems;
    }

    public List<NavigationItem> getNavigationItems(final String requestUri) {
        return new AbstractList<NavigationItem>() {
            @Override
            public NavigationItem get(int index) {
                NavigationItem item = navigationItems.get(index);
                if (StringUtils.equals(item.getUrl(), requestUri)) {        //item is active
                    return new ActiveNavigationItem(item);
                } else {
                    return item;
                }
            }

            @Override
            public int size() {
                return navigationItems.size();
            }
        };
    }

    private class ActiveNavigationItem implements NavigationItem {
        private final NavigationItem delegate;

        private ActiveNavigationItem(NavigationItem delegate) {
            this.delegate = delegate;
        }

        @Override
        public String getUrl() {
            return delegate.getUrl();
        }

        @Override
        public String getName() {
            return delegate.getName();
        }

        @Override
        public boolean isActive() {
            return true;
        }
    }
}
