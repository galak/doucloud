package doucloud.ui.navigation;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class DefaultNavigationItem implements NavigationItem {
    private final String url;

    private final String name;

    public DefaultNavigationItem(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isActive() {
        return false;
    }
}
