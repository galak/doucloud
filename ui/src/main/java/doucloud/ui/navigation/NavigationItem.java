package doucloud.ui.navigation;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public interface NavigationItem {
    String getUrl();

    String getName();

    boolean isActive();
}
