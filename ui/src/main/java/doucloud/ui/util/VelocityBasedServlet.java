package doucloud.ui.util;

import doucloud.ui.navigation.Navigator;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.generic.DateTool;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public abstract class VelocityBasedServlet extends HttpServlet {
    public static final String DEFAULT_TEMPLATE_LOCATION = "/doucloud/ui/templates/";

    private final String templateName;

    private final String templateLocation;

    @Inject
    private Navigator navigator;

    private Template tpl;

    public VelocityBasedServlet(String templateName) {
        this(templateName, DEFAULT_TEMPLATE_LOCATION);
    }

    public VelocityBasedServlet(String templateName, String templateLocation) {
        this.templateName = templateName;
        this.templateLocation = templateLocation;
    }

    @Inject
    public void initTemplate(VelocityEngine engine) throws Exception {
        String templatePath = templateLocation + templateName;
        this.tpl = engine.getTemplate(templatePath);
    }

    private VelocityContext initContext(HttpServletRequest req) {
        VelocityContext ctx = new VelocityContext();
        ctx.put("navItems", navigator.getNavigationItems(req.getRequestURI()));
        ctx.put("dateTool", new DateTool());

        ctx.put("servletContextUrl", UrlUtils.getServletContextUrl(req));

        return ctx;
    }

    protected abstract void prepareContext(HttpServletRequest req, HttpServletResponse resp, VelocityContext ctx) throws IOException;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        VelocityContext ctx = initContext(req);
        prepareContext(req, resp, ctx);
        renderAndClose(req, resp, ctx);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        VelocityContext ctx = initContext(req);
        prepareContext(req, resp, ctx);
        renderAndClose(req, resp, ctx);
    }

    protected void render(Writer writer, VelocityContext ctx) throws IOException {
        tpl.merge(ctx, writer);
    }

    protected void renderAndClose(HttpServletRequest req, HttpServletResponse resp, VelocityContext ctx) throws IOException {
        Writer writer = null;
        try {
            resp.setCharacterEncoding("utf-8");
            resp.setContentType(getMimeType(req));
            resp.setStatus(HttpServletResponse.SC_OK);

            writer = resp.getWriter();

            render(writer, ctx);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    protected String getMimeType(HttpServletRequest req) {
        String mimeType = getServletContext().getMimeType(req.getPathInfo());
        if (mimeType == null) {
            return "text/html";
        } else {
            return mimeType;
        }
    }
}
