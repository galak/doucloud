package doucloud.ui.util;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class UrlUtils {
    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    public static final String URL_SEGMENTS_DELIMITER = "/";
    public static final String PROTOCOL_DELIMITER = "://";

    public static String getServletContextUrl(HttpServletRequest req) {
        StringBuilder url = new StringBuilder();
        String scheme = req.getScheme();
        int port = req.getServerPort();
        String contextPath = req.getContextPath();

        url.append(scheme);
        url.append(PROTOCOL_DELIMITER);
        url.append(req.getServerName());
        if ((scheme.equals(HTTP) && port != 80)
                || (scheme.equals(HTTPS) && port != 443)) {
            url.append(':');
            url.append(req.getServerPort());
        }

        url.append(contextPath);

        return url.toString();
    }

    public static String getServletPath(HttpServletRequest req, String path) {
        String url = getServletContextUrl(req);
        if (!url.endsWith("/")) {
            url += URL_SEGMENTS_DELIMITER;
        }
        url += path;

        return url;
    }
}
