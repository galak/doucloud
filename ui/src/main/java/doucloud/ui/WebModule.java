package doucloud.ui;

import com.google.inject.servlet.ServletModule;
import doucloud.ui.navigation.DefaultNavigationItem;
import doucloud.ui.navigation.NavigationItem;
import doucloud.ui.navigation.Navigator;
import doucloud.ui.webstart.GetArtifactServlet;
import doucloud.ui.webstart.GetDescriptorServlet;
import doucloud.ui.webstart.WebStartServlet;
import org.apache.velocity.app.VelocityEngine;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class WebModule extends ServletModule {
    private static final String downloadContext = "get/";
    private static final String jnlpName = "hackathon-cloud.jnlp";
    private static final String jarName = "hackathon-cloud.jar";

    private final int httpPort;

    public WebModule(int httpPort) {
        this.httpPort = httpPort;
    }

    @Override
    protected void configureServlets() {
        configureVelocity();
        configureNavigation();

        serve("/members").with(MembershipServlet.class);
        serve("/assets/*").with(WebResourcesServlet.class);

        configureDownloadArtifact();
    }

    private void configureDownloadArtifact() {
        String jnlpPath = downloadContext + jnlpName;

        serve("/" + jnlpPath).with(new GetDescriptorServlet(downloadContext, jarName, jnlpName, httpPort));
        serve("/" + downloadContext + jarName).with(GetArtifactServlet.class);
        serve("/join").with(new WebStartServlet(jnlpPath));
    }

    private void configureVelocity() {
        try {
            InputStream is = getClass().getResourceAsStream("/velocity.properties");

            Properties p = new Properties();
            p.load(is);

            VelocityEngine engine = new VelocityEngine(p);
            engine.init();
            bind(VelocityEngine.class).toInstance(engine);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void configureNavigation() {
        final String contextPath = getServletContext().getContextPath();

        List<NavigationItem> items = new ArrayList<NavigationItem>() {{
            add(create(contextPath, "", "Home"));
            add(create(contextPath, "members", "Cluster members"));
            add(create(contextPath, "join", "Join cluster"));
        }};

        Navigator navigator = new Navigator(items);

        bind(Navigator.class).toInstance(navigator);
    }

    private static NavigationItem create(String contextPath, String itemPath, String name) {
        return new DefaultNavigationItem(contextPath + "/" + itemPath, name);
    }
}
