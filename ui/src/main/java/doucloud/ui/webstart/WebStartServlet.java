package doucloud.ui.webstart;

import doucloud.ui.util.VelocityBasedServlet;
import org.apache.velocity.VelocityContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class WebStartServlet extends VelocityBasedServlet {
    private final String jnlpPath;

    public WebStartServlet(String jnlpPath) {
        super("webstart.vm");

        this.jnlpPath = jnlpPath;
    }

    @Override
    protected void prepareContext(HttpServletRequest req, HttpServletResponse resp, VelocityContext ctx) throws IOException {
        ctx.put("jnlpPath", jnlpPath);
    }
}
