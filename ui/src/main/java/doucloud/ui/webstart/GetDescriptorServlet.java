package doucloud.ui.webstart;

import doucloud.ui.util.UrlUtils;
import doucloud.ui.util.VelocityBasedServlet;
import org.apache.velocity.VelocityContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class GetDescriptorServlet extends VelocityBasedServlet {
    private final String downloadContextPath;

    private final String jarName;

    private final String jnlpName;

    private final int httpPort;

    public GetDescriptorServlet(String downloadContextPath, String jarName, String jnlpName, int httpPort) {
        super("webstart-jnlp.vm");
        this.downloadContextPath = downloadContextPath;
        this.jarName = jarName;
        this.jnlpName = jnlpName;
        this.httpPort = httpPort;
    }

    @Override
    protected void prepareContext(HttpServletRequest req, HttpServletResponse resp, VelocityContext ctx) throws IOException {
        ctx.put("codebase", UrlUtils.getServletPath(req, downloadContextPath));
        ctx.put("jarName", jarName);
        ctx.put("jnlpName", jnlpName);
        ctx.put("httpPort", httpPort);

        resp.setHeader("Content-Disposition", "attachment; filename=" + jnlpName);
    }
}
