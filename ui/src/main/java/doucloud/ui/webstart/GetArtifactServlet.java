package doucloud.ui.webstart;

import org.apache.commons.io.IOUtils;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
@Singleton
public class GetArtifactServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fileName = getFilename(req);

        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        resp.setContentType("application/java-archive");

        URL jarUrl = getClass().getResource("/anchor.txt");
        JarURLConnection conn = (JarURLConnection) jarUrl.openConnection();
        URLConnection jarConn = conn.getJarFileURL().openConnection();
        resp.setContentLength(jarConn.getContentLength());

        InputStream jarStream = null;
        OutputStream outputStream = null;
        try {
            jarStream = jarConn.getInputStream();
            outputStream = resp.getOutputStream();

            IOUtils.copy(jarStream, outputStream);
        } finally {
            IOUtils.closeQuietly(jarStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    private String getFilename(HttpServletRequest req) {
        String path = req.getRequestURI();
        String[] pathElements = path.split("/");

        if (pathElements.length > 1) {
            return pathElements[pathElements.length - 1];
        } else {
            return "hackathon-cloud.jar";
        }
    }
}
