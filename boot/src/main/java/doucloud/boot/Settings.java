package doucloud.boot;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class Settings {
    public static final String HTTP_PORT_RANGE = "HTTP port should be in range 1-65535";

    public static int getHttpPort() {
        Integer result = null;

        String portString = System.getProperty("http.port");
        result = parsePort(portString);

        if (result == null) {
            portString = System.getProperty("jnlp.http.port");
            result = parsePort(portString);
        }

        if (result == null) {
            result = 8181;
        }

        return result;
    }

    private static Integer parsePort(String portString) {
        if (portString != null) {
            try {
                int port = Integer.parseInt(portString);
                if (port < 1 || port > 65535) {
                    throw new RuntimeException(HTTP_PORT_RANGE);
                }

                return port;
            } catch (NumberFormatException e) {
                throw new RuntimeException(HTTP_PORT_RANGE);
            }
        } else {
            return null;
        }
    }
}
