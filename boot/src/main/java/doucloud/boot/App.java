package doucloud.boot;

import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class App {
    private final Server server;

    private App() throws Exception {
        server = new Server();
        SelectChannelConnector connector = new SelectChannelConnector();
        connector.setPort(Settings.getHttpPort());
        server.addConnector(connector);

        ServletContextHandler handler = new ServletContextHandler();
        handler.addEventListener(new DouServletContextListener());
        handler.addFilter(GuiceFilter.class, "/*", 0);
        handler.addServlet(DefaultServlet.class, "/*");

        handler.setMimeTypes(initMimeTypes());
        server.setHandler(handler);

        server.start();
        server.join();
    }

    public static void main(String[] args) throws Exception {
        new App();
    }

    private static MimeTypes initMimeTypes() {
        MimeTypes mimeTypes = new MimeTypes();
        mimeTypes.addMimeMapping("js", "application/x-javascript");
        mimeTypes.addMimeMapping("css", "text/css");
        mimeTypes.addMimeMapping("png", "image/png");
        return mimeTypes;
    }
}
