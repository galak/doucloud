package doucloud.boot;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import doucloud.ui.WebModule;
import duocloud.core.CoreModule;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class DouServletContextListener extends GuiceServletContextListener {
    @Override
    protected Injector getInjector() {
        CoreModule coreModule = new CoreModule();
        WebModule webModule = new WebModule(Settings.getHttpPort());

        return Guice.createInjector(coreModule, webModule);
    }
}
