package duocloud.core;

import com.hazelcast.core.Cluster;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.IMap;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;
import com.hazelcast.core.Member;

import javax.inject.Singleton;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author Anatoliy Sokolenko
 */
@Singleton
public class MembershipService implements LifecycleListener {
    public static final String MEMBERS_INFO_MAP = "membersInfo";

    public MembershipService() {
        Hazelcast.getLifecycleService().addLifecycleListener(this);
        refreshOwnInformation();
    }

    public Set<MemberInfo> getMembers() {
        Set<Member> members = Hazelcast.getCluster().getMembers();
        Set<MemberInfo> result = new HashSet<MemberInfo>();

        IMap<String, MemberRecord> records = Hazelcast.getMap(MEMBERS_INFO_MAP);

        for (Member member : members) {
            MemberRecord memberRecord = null;
            if (records.containsKey(member.getUuid())) {
                memberRecord = records.get(member.getUuid());
            }

            result.add(new MemberInfo(member, memberRecord));
        }

        return result;
    }

    @Override
    public void stateChanged(LifecycleEvent event) {
        switch (event.getState()) {
            case STARTED:
            case RESTARTED:
                refreshOwnInformation();
        }
    }

    private void refreshOwnInformation() {
        Cluster cluster = Hazelcast.getCluster();
        Member local = cluster.getLocalMember();

        Map<String, MemberRecord> memberRecords = Hazelcast.getMap(MEMBERS_INFO_MAP);
        memberRecords.put(local.getUuid(), buildMemberRecord(cluster));
    }

    private MemberRecord buildMemberRecord(Cluster cluster) {
        Runtime runtime = Runtime.getRuntime();
        Properties properties = System.getProperties();

        return new MemberRecord(
                runtime.availableProcessors(),
                properties.getProperty("os.version"),
                properties.getProperty("os.name"),
                properties.getProperty("os.arch"),
                properties.getProperty("java.vm.vendor"),
                properties.getProperty("java.vm.version"),
                cluster.getClusterTime());
    }
}
