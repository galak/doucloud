package duocloud.core;

import com.google.inject.AbstractModule;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class CoreModule extends AbstractModule {
    @Override
    protected void configure() {

    }
}
