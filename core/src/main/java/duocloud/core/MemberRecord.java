package duocloud.core;

import com.hazelcast.nio.DataSerializable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
class MemberRecord implements DataSerializable {
    private int processors;

    private String osVersion;

    private String osName;

    private String osArch;

    private String javaVmVendor;

    private String javaVmVersion;

    private long joinedTime;

    private MemberRecord() {
    }

    MemberRecord(int processors, String osVersion, String osName, String osArch,
                 String javaVmVendor, String javaVmVersion, long joinedTime) {
        this.processors = processors;
        this.osVersion = osVersion;
        this.osName = osName;
        this.osArch = osArch;
        this.javaVmVendor = javaVmVendor;
        this.javaVmVersion = javaVmVersion;
        this.joinedTime = joinedTime;
    }

    public int getProcessors() {
        return processors;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsArch() {
        return osArch;
    }

    public String getJavaVmVendor() {
        return javaVmVendor;
    }

    public String getJavaVmVersion() {
        return javaVmVersion;
    }

    public long getJoinedTime() {
        return joinedTime;
    }

    @Override
    public void writeData(DataOutput out) throws IOException {
        out.writeInt(processors);
        out.writeUTF(osVersion);
        out.writeUTF(osName);
        out.writeUTF(osArch);
        out.writeUTF(javaVmVendor);
        out.writeUTF(javaVmVersion);
        out.writeLong(joinedTime);
    }

    @Override
    public void readData(DataInput in) throws IOException {
        processors = in.readInt();
        osVersion = in.readUTF();
        osName = in.readUTF();
        osArch = in.readUTF();
        javaVmVendor = in.readUTF();
        javaVmVersion = in.readUTF();
        joinedTime = in.readLong();
    }
}
