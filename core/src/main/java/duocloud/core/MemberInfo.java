package duocloud.core;

import com.hazelcast.core.Member;

import java.net.InetAddress;
import java.util.Calendar;

/**
 * @author Anatoliy Sokolenko (Grid Dynamics, Inc.)
 */
public class MemberInfo {
    private final Member member;

    private final MemberRecord memberRecord;

    public MemberInfo(Member member, MemberRecord memberRecord) {
        this.member = member;
        this.memberRecord = memberRecord;
    }

    public boolean isLocalMember() {
        return member.localMember();
    }

    public InetAddress getInetAddress() {
        return member.getInetSocketAddress().getAddress();
    }

    public int getInetPort() {
        return member.getInetSocketAddress().getPort();
    }

    public String getUuid() {
        return member.getUuid();
    }

    public boolean isLiteMember() {
        return member.isLiteMember();
    }

    public Calendar getJoinedTime() {
        if (memberRecord != null) {
            long joinedTime = memberRecord.getJoinedTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(joinedTime);

            return calendar;
        } else {
            return null;
        }
    }

    public Integer getProcessors() {
        if (memberRecord != null) {
            return memberRecord.getProcessors();
        } else {
            return null;
        }
    }

    public String getOsVersion() {
        if (memberRecord != null) {
            return memberRecord.getOsVersion();
        } else {
            return null;
        }
    }

    public String getOsName() {
        if (memberRecord != null) {
            return memberRecord.getOsName();
        } else {
            return null;
        }
    }

    public String getOsArch() {
        if (memberRecord != null) {
            return memberRecord.getOsArch();
        } else {
            return null;
        }
    }

    public String getJavaVmVendor() {
        if (memberRecord != null) {
            return memberRecord.getJavaVmVendor();
        } else {
            return null;
        }
    }

    public String getJavaVmVersion() {
        if (memberRecord != null) {
            return memberRecord.getJavaVmVersion();
        } else {
            return null;
        }
    }
}
